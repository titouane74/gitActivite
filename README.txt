------
README
------

Ce projet a pour but d'utiliser les commande git apprise dans le cours.

Pour cela on doit :
-------------------
- Créer un repository Git simple contenant
- Un fichier nommé README.txt (c'est moi !! :o))
- 2 autres fichiers de notre choix
- 4 commits distincts montrant l'ajout puis la modification de certains fichiers.
- Envoyer le repository sur GitHub
- Copier l'URL du repository GitHub au format https
- Télécharger un document
- Y ajouter notre adresse https après le texte 'L'URL de mon repository sur GitHub
- Zipper ce fichier et l'envoyer pour valider l'activité.